@extends('master')

@section('title')
    <h1 style="text-align: center">Comments</h1>
@stop


@section('form')
    @if (Auth::check()) 
        {{ Form::open(array('action' => 'CommentController@store', 'files' => true)) }}
            {{ Form::hidden('post_id', $post->id) }}
            {{ Form::label('message', 'Comment: ', array('class' => 'block')) }}
            {{ Form::text('message', '', array('class' => 'form-control', 'placeholder' => 'Comment')) }}
            {{ $errors->first('message') }}
            <p></p>
            {{ Form::submit('Comment', array('class' => 'btn btn-primary postbtn')) }}
        {{ Form::close() }}
    @else
        <h2>Please Login</h2>
        {{ Form::open(array('action' => 'UserController@login')) }}
            {{ Form::label('loginemail', 'Email: ', array('class' => 'block')) }}
            {{ Form::text('loginemail', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
            <p></p>
            {{ Form::label('loginpassword', 'Password: ', array('class' => 'block')) }}
            {{ Form::password('loginpassword', array('class' => 'form-control', 'placeholder' => 'Password')) }}
            <p></p>
            {{ Form::submit('Login', array('class' => 'btn btn-primary postbtn')) }}
        {{ Form::close() }}
    @endif
@stop

@section('content')
    <div>
        <div>
            <div class='post'>
                <div class="imagediv">
                    <!-- Small fragment of PHP for correcting the path returned from image->url() -->
                    <!-- Path from this function is returned in format "/xxx/xxx/xxx.xxx", but file_exists requires a path without leading slash -->
                    <!-- Used ltrim to edit -->
                    <?php
                        $imageloc =  User::find($post->user_id)->image->url();
                        $fixedloc = ltrim($imageloc, '/');
                    ?>
                    @if ( File::exists($fixedloc))
                        <a href="{{ route('user.show', $post->user_id) }}">{{ HTML::image($fixedloc, "image", array("class" => "postimage")) }}</a>
                    @else
                         <a href="{{ route('user.show', $post->user_id) }}">{{ HTML::image("images/noimage.jpg", "image", array("class" => "postimage")) }}</a>
                    @endif
                </div>
                <div class="textwrap">
                    <a href="{{ route('user.show', $post->user_id) }}"> <label class="username" style="color: black; cursor: pointer"> {{{Post::find($post->id)->user->firstname . ' ' . Post::find($post->id)->user->lastname}}} </label></a><br>
                    <label class="title"> {{{$post->title}}} </label><br>
                    <label class="message"> {{{$post->message}}} </label>
                </div>
                <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                    <a href='{{{ url("post/$post->id") }}}'><button class="btn btn-info" type="button">Comments <span class="badge">{{{$commentcount}}}</span></button></a>
                    <a href='{{{ url("post/$post->id/edit") }}}'><button class="btn btn-warning">Edit</button></a>
                    {{ Form::open(array('route' => array('post.destroy', $post->id), 'method' => 'delete', 'class' => 'inline')) }}
                        <button type="submit" class="btn btn-danger">Delete</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div>
        @foreach ($comments as $comment)
            <div>
                <div class='post'>
                    <div class="textwrap">
                        <a href="{{ route('user.show', $post->user_id) }}"> <label class="username" style="color: black; cursor: pointer"> {{{Comment::find($comment->id)->user->firstname . ' ' . Comment::find($comment->id)->user->lastname}}} </label></a><br>
                        <label class="message">{{{$comment->message}}} </label>
                    </div>
                    <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                        {{ Form::open(array('route' => array('comment.destroy', $comment->id), 'method' => 'delete', 'class' => 'inline')) }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        @endforeach
        {{ $comments->links() }}
    </div>
@stop