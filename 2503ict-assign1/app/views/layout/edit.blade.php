@extends('layout.master')

@section('form')
    <label></label>
    <form method="post" action='{{{ url("editPost") }}}'>
        <div class="form-group">
            <label for "inputName">Name:</label><br>
            <input class="form-control" id="inputName" type="text" name="name" placeholder="Input name here." value='{{{ $post->User }}}'>
        </div>
        <div class="form-group">
            <label for "inputTitle">Title:</label><br>
            <input class="form-control" id="inputTitle" type="text" name="title" placeholder="Input title here." value='{{{ $post->Title }}}'>
        </div>
        <div class="form-group">
            <label for "inputMessage">Message:</label><br>
            <textarea class="form-control" rows="4" cols="10" name="message" placeholder="Input content here." >{{{ $post->Message }}}</textarea>
        </div>
        <div class="form-group">
            <label class="label label-warning">Image cannot be edited</label><br>
        </div>
        <input type="hidden" name="id" value="{{{ $post->Id }}}">
        <button type="submit" class="btn btn-primary postbtn">Save</button>
        <a href='{{{ url("home") }}}'><button type="button" class="btn btn-default">Cancel</button></a>
    </form>
@stop

@section('content')
    <div>
        <div class='post'>
            <div class="imagediv">
                @if ( File::exists(url("images/$post->Id.jpg")))
                {{ HTML::image("images/$post->Id.jpg", "image", array("class" => "postimage")) }}
                @else
                {{ HTML::image("images/noimage.jpg", "image", array("class" => "postimage")) }}
                @endif
            </div>
            <div class="textwrap">
                <label class="username"> {{{$post->User}}} </label><br>
                <label class="title"> {{{$post->Title}}} </label><br>
                <label class="message"> {{{$post->Message}}} </label>
            </div>
            <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                <a href='{{{ url("comments/$post->Id") }}}'><button class="btn btn-info" type="button">Comments <span class="badge">{{{$commentcount[$post->Id]}}}</span></button></a>
                <a href='{{{ url("edit/$post->Id") }}}'><button class="btn btn-warning">Edit</button></a>
                <a href='{{{ url("deletep/$post->Id") }}}'><button class="btn btn-danger">Delete</button></a>
            </div>
        </div>
    </div>
@stop