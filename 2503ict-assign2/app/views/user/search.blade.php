@extends('master')

@section('title')
    <h1 style="text-align: center">Results</h1>
@stop

@section('content')
    @foreach ($users as $user)
        <a href="{{ route('user.show', $user->id) }}" style="display:block" > <div class='post'>
            <div class="imagediv">
                <!-- Small fragment of PHP for correcting the path returned from image->url() -->
                <!-- Path from this function is returned in format "/xxx/xxx/xxx.xxx", but file_exists requires a path without leading slash -->
                <!-- Used ltrim to edit -->
                <?php
                    $imageloc =  User::find($user->id)->image->url();
                    $fixedloc = ltrim($imageloc, '/');
                ?>
                @if ( File::exists($fixedloc))
                    {{ HTML::image($fixedloc, "image", array("class" => "postimage")) }}
                @else
                     {{ HTML::image("images/noimage.jpg", "image", array("class" => "postimage")) }}
                @endif
            </div>
            <div class="textwrap">
                <label class="username" style="color: black; cursor: pointer"> {{{$user->firstname . ' ' . $user->lastname}}} </label><br>
            </div>
        </div> </a>
    @endforeach
@stop