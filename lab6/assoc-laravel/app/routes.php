<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $query = Input::get('query');

  $results = search($query);

	return View::make('pms.results')->withPms($results)->withQuery($query);
});


/* Functions for PM database example. */

/* Search sample data for $name or $year or $state from form. */
function search($query) {

  // Filter $pms by $name
  if (!empty($query)) {
    $sql = "select * from pms where name like ? OR party like ? OR state like ? or duration like ? or start like ? or finish like ?";
    $pms = DB::select($sql, array("%$query%", "%$query%", "%$query%", "%$query%", "%$query%", "%$query%"));
  }
  return $pms;
  
}