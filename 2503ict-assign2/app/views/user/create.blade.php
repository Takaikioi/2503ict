@extends('master')

@section('form')
    @if (Auth::check()) 
        {{ Form::open(array('action' => 'UserController@login')) }}
            {{ Form::label('loginemail', 'Email: ', array('class' => 'block')) }}
            {{ Form::text('loginemail', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
            <p></p>
            {{ Form::label('loginpassword', 'Password: ', array('class' => 'block')) }}
            {{ Form::text('loginpassword', '', array('class' => 'form-control', 'placeholder' => 'Password')) }}
            <p></p>
            {{ Form::submit('Login', array('class' => 'btn btn-primary postbtn')) }}
        {{ Form::close() }}
    @else
        <h2>Please Login</h2>
        {{ Form::open(array('action' => 'UserController@login')) }}
            {{ Form::label('loginemail', 'Email: ', array('class' => 'block')) }}
            {{ Form::text('loginemail', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
            <p></p>
            {{ Form::label('loginpassword', 'Password: ', array('class' => 'block')) }}
            {{ Form::password('loginpassword', array('class' => 'form-control', 'placeholder' => 'Password')) }}
            <p></p>
            {{ Form::submit('Login', array('class' => 'btn btn-primary postbtn')) }}
        {{ Form::close() }}
    @endif
@stop

@section('content')
    {{ Form::open(array('action' => 'UserController@store', 'files' => true)) }}
        {{ Form::label('firstname', 'Name: ', array('class' => 'block')) }}
        {{ Form::text('firstname', '', array('class' => 'inline name form-control', 'placeholder' => 'First Name')) }}
        {{ $errors->first('firstname') }}
        {{ Form::text('lastname', '', array('class' => 'inline name form-control', 'placeholder' => 'Last Name')) }}
        {{ $errors->first('lastname') }}
        <p></p>
        {{ Form::label('email', 'Email: ', array('class' => 'block')) }}
        {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
        {{ $errors->first('email') }}
        <p></p>
        {{ Form::label('dob', 'Date of Birth: ', array('class' => 'block')) }}
        {{ Form::text('dob', '', array('class' => 'form-control', 'placeholder' => 'Date of Birth')) }}
        {{ $errors->first('dob') }}
        <p></p>
        {{ Form::label('password', 'Password: ', array('class' => 'block')) }}
        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'New Password')) }}
        {{ $errors->first('password') }}
        <p></p>
        {{ Form::file('image') }}
        <p></p>
        {{ Form::submit('Create Account', array('class' => 'btn btn-primary postbtn')) }}
    {{ Form::close() }}
@stop