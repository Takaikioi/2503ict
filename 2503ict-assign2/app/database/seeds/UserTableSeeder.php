<?php
    class UserTableSeeder extends Seeder {
        function run() {
            $user = new User;
            $user->email = 'email@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'First';
            $user->lastname = 'Last';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email2@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'Terry';
            $user->lastname = 'Last';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email3@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'Suzie';
            $user->lastname = 'Last';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email4@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'North';
            $user->lastname = 'West';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email5@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'South';
            $user->lastname = 'East';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email6@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'Eric';
            $user->lastname = 'First';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email7@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'Stephanie';
            $user->lastname = 'First';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email8@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'Stella';
            $user->lastname = 'Stardust';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email9@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'Thomas';
            $user->lastname = 'Grick';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
            
            $user = new User;
            $user->email = 'email10@gmail.com';
            $user->password = Hash::make('password');
            $user->firstname = 'Tina';
            $user->lastname = 'Test';
            $user->dob = new DateTime('11/01/1996');
            $user->save();
        }
    }
?>