<?php
  $posts = array(
    array('date'=>'12/12/12', 'message'=>'Message1', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message2', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message3', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message4', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message5', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message6', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message7', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message8', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message9', 'image'=>'images/Cat.jpg'),
    array('date'=>'12/12/12', 'message'=>'Message10', 'image'=>'images/Cat.jpg')
  );
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/navbar.css" rel="stylesheet">
    
    <!-- Stylesheet for this webpage -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Social Network</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href = "">Photos</a></li>
              <li><a href = "">Friends</a></li>
              <li><a href = "">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      
      <!-- Main body -->
      <div class="row">
        <div class="col-sm-4">
          <form>
            <div class="form-group">
              <label for "inputName">Name:</label><br>
              <input class="form-control" id="inputName" type="text" name="name" placeholder="Your name here.">
            </div>
            <div class="form-group">
              <label for "inputMessage">Message:</label><br>
              <textarea class="form-control" rows="4" cols="10" placeholder="Your message here."></textarea>
            </div>
            <button id="btnpost" type="submit" class="btn btn-primary">Post</button>
          </form>
        </div>
        
        <div class="col-sm-8">
          <div class='post'>
            <img class='postimage' src='images\zuckerberg.jpg' alt='photo'>
            <label>Mark Zuckerberg</label><br>
            <?php 
              $num = rand(1, count($posts));
              echo "<label>$num</label><br>";
            ?>
          </div>
          <?php
            for($i=0; $i < $num; $i++){
              $image = $posts[$i]['image'];
              $date = $posts[$i]['date'];
              $message = $posts[$i]['message'];
              echo "<div class='post'>
                      <img class='postimage' src='$image' alt='photo'>
                      <label>Date: $date </label><br>
                      <label>Message: $message </label>
                    </div>";
            }
          ?>
        </div>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
