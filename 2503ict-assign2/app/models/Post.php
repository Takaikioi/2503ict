<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Post extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait, EloquentTrait;

	//The database table used by the model.
	protected $table = 'posts';
	
	public function __construct(array $attributes = array()) {
		$this->hasAttachedFile('image', [
			'styles' => [
			    'large' => '500x500',
				'medium' => '300x300',
				'thumb' => '100x100'
			]
		]);
		
		parent::__construct($attributes);
	}
	
	public function user() {
	    return $this->belongsTo('User', 'user_id', 'id');
	}
	
	public function comments() {
		return $this->hasMany('Comment');
	}
	
	public static $rules = array(
       	'title' => 'required',
        'message' => 'required',
        'privacy' => 'required'
    );
}
