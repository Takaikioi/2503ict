<?php
/* Australian Prime Ministers.  Data as of 5 March 2010. */
function getPms()
{
  $pms = array(
      array('index' => '1', 'name' => 'Edmund Barton',  'address' => 'New South Wales', 'phone' => '0404782192', 'email' => 'eddy@sweetpmnetwork.net.au'),
      array('index' => '2', 'name' => 'Alfred Deakin',  'address' => 'Victoria', 'phone' => '0404782193', 'email' => 'alfyd@sweetpmnetwork.net.au')
  );
  return $pms;
}
?>

