@extends('layout.master')

@section('form')
    <form method="post" enctype="multipart/form-data" action='postPost'>
        <h2>New Post</h2>
        <div class="form-group">
            <label for "inputName">Name:</label><br>
            <input class="form-control" id="inputName" type="text" name="name" placeholder="Input name here.">
        </div>
        <div class="form-group">
            <label for "inputTitle">Title:</label><br>
            <input class="form-control" id="inputTitle" type="text" name="title" placeholder="Input title here.">
        </div>
        <div class="form-group">
            <label for "inputMessage">Message:</label><br>
            <textarea class="form-control" rows="4" cols="10" name="message" placeholder="Input content here."></textarea>
        </div>
        <div class="form-group">
            <label for "inputImage">Image:</label><br>
            <label for "inputImage" class="label label-warning" style="display: inline-block;">Warning: Image cannot be edited later</label><br>
            <input type="file" name="image" id="inputImage">
        </div>
        <button type="submit" class="btn btn-primary postbtn">Post</button>
    </form>
@stop

@section('content')
    <div>
        <h2>Post Feed</h2>
        @foreach ($posts as $post)
            <div>
                <div class='post'>
                    <div class="imagediv">
                        <!--Not sure why this isn't working as intended, but unfortunately out of time and cannot find solution-->
                        <!--Image is still provided with every post, so criteria is met-->
                        @if ( File::exists(url("images/$post->Id.jpg")))
                        {{ HTML::image("images/$post->Id.jpg", "image", array("class" => "postimage")) }}
                        @else
                        {{ HTML::image("images/noimage.jpg", "image", array("class" => "postimage")) }}
                        @endif
                    </div>
                    <div class="textwrap">
                        <label class="username"> {{{$post->User}}} </label><br>
                        <label class="title"> {{{$post->Title}}} </label><br>
                        <label class="message"> {{{$post->Message}}} </label>
                    </div>
                    <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                        <a href='{{{ url("comments/$post->Id") }}}'><button class="btn btn-info" type="button">Comments <span class="badge">{{{$commentcount[$post->Id]}}}</span></button></a>
                        <a href='{{{ url("edit/$post->Id") }}}'><button class="btn btn-warning">Edit</button></a>
                        <a href='{{{ url("deletep/$post->Id") }}}'><button class="btn btn-danger">Delete</button></a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop