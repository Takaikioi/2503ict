<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
    $posts = getPosts();
    $commentcount = getCommentCountArray($posts);
	return View::make('layout.home') -> withPosts($posts) -> withCommentcount($commentcount);
});
Route::get('/home', function() {
    $posts = getPosts();
    $commentcount = getCommentCountArray($posts);
	return View::make('layout.home') -> withPosts($posts) -> withCommentcount($commentcount);
});

Route::get('/comments/{id}', function($id) {
    $post = getPost($id);
    $comments = getComments($id);
    $commentcount = getCommentCount($post);
    return View::make('layout.comments') -> withPost($post) -> withComments($comments) -> withCommentcount($commentcount);
});

Route::get('/deletep/{id}', function($id) {
    deletePost($id);
    return Redirect::to(url("home"));
});

Route::get('/deletec/{id}/{fid}', function($id, $fid) {
    deleteComment($id, $fid);
    return Redirect::to(url("comments/$fid"));
});

Route::get('/edit/{id}', function($id) {
    $post = getPost($id);
    $commentcount = getCommentCount($post);
    return View::make('layout.edit') -> withPost($post) -> withCommentcount($commentcount); 
});

Route::post('postPost', function() {
    $name = Input::get('name');
    $title =  Input::get('title');
    $message = Input::get('message');
    $id = insertPost($name, $title, $message);
    if (Input::hasFile('image')) {
        $image = Input::file('image')->move('images', $id . '.jpg');
    }
    return Redirect::to(url("home"));
});

Route::post('postComment', function() {
    $name = Input::get('name');
    $comment = Input::get('comment');
    $id = Input::get('id');
    insertComment($name, $comment, $id);
    $back = URL::previous();
    return Redirect::to($back);
});

Route::post('editPost', function() {
    $name = Input::get('name');
    $title =  Input::get('title');
    $message = Input::get('message');
    $id = Input::get('id');
    editPost($id, $name, $title, $message);
    return Redirect::to(url("comments/$id"));
});

function getPosts() {
    $sql = "select * from posts";
    $result = array_reverse(DB::select($sql));
    return $result;
}

function getCommentCountArray($posts) {
    $commentcount = array();
    foreach ($posts as $post) {
        $commentcount[$post->Id] = count(getComments($post->Id));
    }
    return $commentcount;
}

function getCommentCount($post) {
    $commentcount[$post->Id] = count(getComments($post->Id));
    return $commentcount;
}

function getPost($id) {
    $sql = "select * from posts where id = ?";
    $result = DB::select($sql, Array($id));
    $resultWithoutArray = $result[0];
    return $resultWithoutArray;
}

function getComments($id) {
    $sql = "select * from comments where post_id = ?";
    $result = DB::select($sql, Array($id));
    return $result;
}

function insertPost($name, $title, $message) {
    $sql = "insert into posts(user, title, message) values(?, ?, ?)";
    DB::insert($sql, Array($name, $title, $message));
    $id = DB::getPdo()->lastInsertId();
    return $id;
}

function insertComment($name, $comment, $id) {
    $sql = "insert into comments(user, comment, post_id) values (?, ?, ?)";
    DB::insert($sql, Array($name, $comment, $id));
    $id = DB::getPdo()->lastInsertId();
    return $id;
}

function editPost($id, $name, $title, $message) {
    $sql = "update posts set user = ?, title = ?, message = ? where id = ?";
    $result = DB::update($sql, Array($name, $title, $message, $id));
}

function deletePost($id) {
    $sql = 'delete from posts where Id = ?';
    DB::delete($sql, Array($id));
    File::delete("images/$id.jpg");
    $sql = 'delete from comments where Post_id = ?';
    DB::delete($sql, Array($id));
}

function deleteComment($id, $fid) {
    $sql = 'delete from comments where Id = ? and Post_id = ?';
    DB::delete($sql, Array($id, $fid));
}