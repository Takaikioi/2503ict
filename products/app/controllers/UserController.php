<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$v = Validator::make($input, User::$rules);
		
		if ($v->passes()) {
			$password = $input['password'];
			$hashed = Hash::make($password);
			$user = new User;
			$user->username = $input['username'];
			$user->password = $hashed;
			$user->save();
			return Redirect::to(route('product.index'));
		} else {
			return Redirect::to(URL::previous())->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function login() {
		$userdata = array(
			'username' => Input::get('loginusername'),
			'password' => Input::get('loginpassword')
		);
		
		if (Auth::attempt($userdata)) {
			Session::put('login_error', '');
			return Redirect::to(URL::previous());
		} else {
			Session::put('login_error', 'Login failed');
			return Redirect::to(URL::previous())->withInput();
		}
	}
	
	public function logout() {
		Auth::logout();
		return Redirect::to(route('product.index'));
	}
}
