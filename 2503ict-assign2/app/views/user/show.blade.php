@extends('master')

@section('title') 
    <h1 style="text-align: center">{{ $user->firstname . ' ' . $user->lastname }}</h1>
@stop

@section('form')
    @if (Auth::check()) 
        @if (Auth::user()->id == $user->id)
            <h2>Update Details</h2>
            {{ Form::model($user, array('method' => 'PUT', 'files' => true, 'route' => array('user.update', $user->id))) }}
                {{ Form::label('firstname', 'Name: ', array('class' => 'block')) }}
                {{ Form::text('firstname', '', array('class' => 'inline name form-control', 'placeholder' => 'First Name')) }}
                {{ Form::text('lastname', '', array('class' => 'inline name form-control', 'placeholder' => 'Last Name')) }}
                {{ $errors->first('firstname') }}</br>
                {{ $errors->first('lastname') }}
                <p></p>
                {{ Form::label('email', 'Email: ', array('class' => 'block')) }}
                {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
                {{ $errors->first('email') }}
                <p></p>
                {{ Form::label('dob', 'Date of Birth: ', array('class' => 'block')) }}
                {{ Form::text('dob', '', array('class' => 'form-control', 'placeholder' => 'Date of Birth')) }}
                {{ $errors->first('dob') }}
                <p></p>
                {{ Form::label('oldpassword', 'Old Password: ', array('class' => 'block')) }}
                {{ Form::password('oldpassword', array('class' => 'form-control', 'placeholder' => 'Old Password')) }}
                {{ $errors->first('oldpassword') }}
                <p></p>
                {{ Form::label('password', 'New Password: ', array('class' => 'block')) }}
                {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'New Password')) }}
                {{ $errors->first('password') }}
                <p></p>
                {{ Form::file('image') }}
                <p></p>
                {{ Form::submit('Update Account', array('class' => 'btn btn-primary postbtn')) }}
            {{ Form::close() }}
        @elseif ($user->friends()->whereRaw('reference_id like ? or referenced_id like ?', array($user->id, $user->id))->count() > 0)
            <h2>You are already friends</h2>
            <p></p>
            <?php
                $dob = new DateTime($user->dob); 
                $age = $dob->diff(new DateTime);
            ?>
            <h3>{{ $user->firstname }} is {{ $age->y }} years old.</h3>
        @else
            <h2>Add this user as a friend!</h2>
            {{ Form::open(array('route' => array('user.friend', $user->id), 'method' => 'get')) }}
                <button type="submit" class="btn btn-default" style="width: 100%; margin-top: 10px">Add</button>
            {{ Form::close() }}
        @endif
    @else
        <h2>Please Login</h2>
        {{ Form::open(array('action' => 'UserController@login')) }}
            {{ Form::label('loginemail', 'Email: ', array('class' => 'block')) }}
            {{ Form::text('loginemail', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
            <p></p>
            {{ Form::label('loginpassword', 'Password: ', array('class' => 'block')) }}
            {{ Form::password('loginpassword', array('class' => 'form-control', 'placeholder' => 'Password')) }}
            <p></p>
            {{ Form::submit('Login', array('class' => 'btn btn-primary postbtn')) }}
        {{ Form::close() }}
    @endif
@stop

@section('content')
    <div>
        <h2>User's Posts</h2>
        @foreach ($posts as $post)
            <div>
                <div class='post'>
                    <div class="imagediv">
                        <!-- Small fragment of PHP for correcting the path returned from image->url() -->
                        <!-- Path from this function is returned in format "/xxx/xxx/xxx.xxx", but file_exists requires a path without leading slash -->
                        <!-- Used ltrim to edit -->
                        <?php
                            $imageloc =  User::find($post->user_id)->image->url();
                            $fixedloc = ltrim($imageloc, '/');
                        ?>
                        @if ( File::exists($fixedloc))
                            <a href="{{ route('user.show', $post->user_id) }}">{{ HTML::image($fixedloc, "image", array("class" => "postimage")) }}</a>
                        @else
                             <a href="{{ route('user.show', $post->user_id) }}">{{ HTML::image("images/noimage.jpg", "image", array("class" => "postimage")) }}</a>
                        @endif
                    </div>
                    <div class="textwrap">
                        <a href="{{ route('user.show', $post->user_id) }}"> <label class="username" style="color: black; cursor: pointer"> {{{Post::find($post->id)->user->firstname . ' ' . Post::find($post->id)->user->lastname}}} </label></a><br>
                        <label class="title"> {{{$post->title}}} </label><br>
                        <label class="message"> {{{$post->message}}} </label>
                    </div>
                    <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                        <a href='{{{ url("post/$post->id") }}}'><button class="btn btn-info" type="button">Comments <span class="badge">{{{$commentcount[$post->id]}}}</span></button></a>
                        <a href='{{{ url("post/$post->id/edit") }}}'><button class="btn btn-warning">Edit</button></a>
                        {{ Form::open(array('route' => array('post.destroy', $post->id), 'method' => 'delete', 'class' => 'inline')) }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        @endforeach
        {{ $posts->links() }}
    </div>
@stop