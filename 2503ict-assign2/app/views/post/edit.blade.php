@extends('master')

@section('title')
    <h1 style="text-align: center">Edit Post</h1>
@stop

@section('form')
    @if (Auth::check()) 
        {{ Form::model($post, array('method' => 'PUT', 'route' => array('post.update', $post->id))) }}
            {{ Form::label('title', 'Title: ', array('class' => 'block')) }}
            {{ Form::text('title', $post->title, array('class' => 'form-control')) }}
            {{ $errors->first('title') }}
            <p></p>
            {{ Form::label('message', 'Message: ', array('class' => 'block')) }}
            {{ Form::text('message', $post->message, array('class' => 'form-control')) }}
            {{ $errors->first('message') }}
            <p></p>
            <div>
                {{ Form::label('privacy', 'Private: ') }}
                {{ Form::radio('privacy', 'private', false, array('class' => 'postbtn')) }}
                <p></p>
                {{ Form::label('privacy', 'Friends: ') }}
                {{ Form::radio('privacy', 'friends', true, array('class' => 'postbtn')) }}
                <p></p>
                {{ Form::label('privacy', 'Public: ') }}
                {{ Form::radio('privacy', 'public', false, array('class' => 'postbtn')) }}
                <p></p>
                {{ $errors->first('privacy') }}
            </div>
            <p></p>
            {{ Form::submit('Save', array('class' => 'btn btn-primary postbtn')) }}
        {{ Form::close() }}
    @else
        <h2>Please Login</h2>
        {{ Form::open(array('action' => 'UserController@login')) }}
            {{ Form::label('loginemail', 'Email: ', array('class' => 'block')) }}
            {{ Form::text('loginemail', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
            <p></p>
            {{ Form::label('loginpassword', 'Password: ', array('class' => 'block')) }}
            {{ Form::password('loginpassword', array('class' => 'form-control', 'placeholder' => 'Password')) }}
            <p></p>
            {{ Form::submit('Login', array('class' => 'btn btn-primary postbtn')) }}
        {{ Form::close() }}
    @endif
@stop

@section('content')
    <div>
        <!--This is a quick and dirty method of allowing the entire div to become a link to the user's profile.-->
        <!--I'm not particularly happy with this solution, but something better would have taken longer, and with the new HTML5 specification, this is now legal.-->
        <a href="{{ route('user.show', $post->user_id) }}" style="display:block" > <div class='post'>
            <div class="imagediv">
               <!-- Small fragment of PHP for correcting the path returned from image->url() -->
                <!-- Path from this function is returned in format "/xxx/xxx/xxx.xxx", but file_exists requires a path without leading slash -->
                <!-- Used ltrim to edit -->
                <?php
                    $imageloc =  User::find($post->user_id)->image->url();
                    $fixedloc = ltrim($imageloc, '/');
                ?>
                @if ( File::exists($fixedloc))
                    <a href="{{ route('user.show', $post->user_id) }}">{{ HTML::image($fixedloc, "image", array("class" => "postimage")) }}</a>
                @else
                     <a href="{{ route('user.show', $post->user_id) }}">{{ HTML::image("images/noimage.jpg", "image", array("class" => "postimage")) }}</a>
                @endif
            </div>
            <div class="textwrap">
                <a href="{{ route('user.show', $post->user_id) }}"> <label class="username" style="color: black; cursor: pointer"> {{{Post::find($post->id)->user->firstname . ' ' . Post::find($post->id)->user->lastname}}} </label></a><br>
                <label class="title" style="color: black; cursor: pointer"> {{{$post->title}}} </label><br>
                <label class="message" style="color: black; cursor: pointer"> {{{$post->message}}} </label>
            </div>
            <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                <a href='{{{ url("post/$post->id") }}}'><button class="btn btn-info" type="button">Comments <span class="badge">{{{$commentcount}}}</span></button></a>
                <a href='{{{ url("post/$post->id/edit") }}}'><button class="btn btn-warning">Edit</button></a>
                {{ Form::open(array('route' => array('post.destroy', $post->id), 'method' => 'delete', 'class' => 'inline')) }}
                    <button type="submit" class="btn btn-danger">Delete</button>
                {{ Form::close() }}
            </div>
        </div> </a>
    </div>
@stop