<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait, EloquentTrait;

	//The database table used by the model.
	protected $table = 'users';

	//The attributes excluded from the model's JSON form.
	protected $hidden = array('remember_token');
	
	public function __construct(array $attributes = array()) {
		$this->hasAttachedFile('image', [
			'styles' => [
				'medium' => '300x300',
				'thumb' => '100x100'
			]
		]);
		
		parent::__construct($attributes);
	}
	
	public function posts() {
		return $this->hasMany('Post')->orderBy('id', 'DESC');
	}
	
	public function friends() {
		return $this->belongsToMany('User', 'friends', 'reference_id', 'referenced_id');
	}
	
	public static $rules = array(
       	'email' => 'required|email|unique:users',
        'password' => 'required|min:5',
        'firstname' => 'required',
        'lastname' => 'required',
        'dob' => 'required|date',
        'image' => 'image'
    );
}
