<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Comment extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use UserTrait, RemindableTrait, EloquentTrait;

	//The database table used by the model.
	protected $table = 'comments';
	
	public function user() {
	    return $this->belongsTo('User', 'user_id', 'id');
	}
	
	public function post() {
	    return $this->belongsTo('Post', 'post_id', 'id');
	}
	
	public static $rules = array(
        'message' => 'required'
    );
}
