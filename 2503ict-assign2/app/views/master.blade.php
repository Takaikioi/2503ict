<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    
    <title>Social Network</title>
    
    <!-- Laravel stylesheet links -->
    
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/style.css') }}
    {{ HTML::style('css/navbar.css') }}
    
    <!-- Laravel script link for boostrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    {{ HTML::script('js/bootstrap.min.js') }}
    
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href='{{{url("home")}}}'>Social Network</a>
            <div class="input-group margin">
              <form class="form-inline" action="{{ route('user.search') }}">
               <input type="text" class="form-control" name="search" placeholder="Search for users">
               <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Search</button>
               </span>
              </form>
            </div>
          <div id="navbar" class="navbar-collapse collapse bar-inline">
            <ul class="nav navbar-nav navbar-right">
              <li><a href = '{{{url("home")}}}'>Home</a></li>
              <li><a href = '{{{url("docs/Site_Diagram.png")}}}'>Site Diagram</a></li>
              <li><a href = '{{{url("docs/WP_ER_Diagram.png")}}}'>ER Diagram</a></li>
              <li><a href = '{{{url("docs/Breakdown.docx")}}}'>Work Breakdown</a></li>
              @if (Auth::check())
                <li><a href = "{{route('user.show', Auth::user()->id) }}">{{ Auth::user()->firstname . ' ' . Auth::user()->lastname }}</a></li>
                <li>{{ link_to_route('user.logout', 'Logout') }}</li>
              @else
                <li><a href = '{{{url('user/create')}}}'>Login/Create Account</a></li>
              @endif
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      
      <!-- Main body -->
      @yield('title')
      <div class="row">
        <div class="col-sm-4">
          @yield('form')
        </div>
        <div class="col-sm-8">
          @yield('content')
        </div>
      </div>
    </div>
  </body>
</html>
