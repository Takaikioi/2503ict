<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{	
		$comment = new Comment;
		$input = Input::all();
	   	$v = Validator::make($input, Comment::$rules);
	    
	    if ($v->passes()) {	
	    	$comment->user_id = Auth::user()->id;
	    	$comment->post_id = $input['post_id'];
			$comment->message = $input['message'];
			$comment->save();
			
			return Redirect::route('post.show', $comment->post_id);
		} else {
			return Redirect::route('user.create')->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Comment::find($id)->delete();
	 	return Redirect::to(URL::previous());
	}

}
