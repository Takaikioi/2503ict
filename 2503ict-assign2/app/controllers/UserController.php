<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = new User;
		$input = Input::all();
		$v = Validator::make($input, User::$rules);
		
		if ($v->passes()) {
			$password = $input['password'];
			$hashed = Hash::make($password);
			$user->email = $input['email'];
			$user->password = $hashed;
			$user->firstname = $input['firstname'];
			$user->lastname = $input['lastname'];
			if (Input::hasFile('image')) {
				$user->image = $input['image'];
			}
			$time = new DateTime($input['dob']);
			$user->dob = $time;
			$user->save();
			return Redirect::to(route('user.show', $user->id));
		} else {
			return Redirect::to(URL::previous())->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{	
		$user = User::find($id);
		$posts = User::find($id)->posts()->paginate(8);
	    $commentcount = array();
	    foreach ($posts as $post) {
	        $commentcount[$post->id] = count(Post::find($post->id)->comments);
	    }
		return View::make('user.show')->withUser($user)->withPosts($posts)->withCommentcount($commentcount);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::find($id);
		$input = Input::all();
		$v = Validator::make($input, User::$rules);
		$password = $input['password'];
		$hashed = Hash::make($password);
		if (Auth::validate(array('password' => $input['oldpassword']))) {
			if (!empty($input['email'])) {
				$user->email = $input['email'];
			}
			if (!empty($input['password'])) {
				$user->password = $hashed;
			}
			if (!empty($input['firstname'])) {
				$user->firstname = $input['firstname'];
			}
			if (!empty($input['lastname'])) {
				$user->lastname = $input['lastname'];
			}
			if (!empty($input['dob'])) {
				$time = new DateTime($input['dob']);
				$user->dob = $time;
			}
			if (Input::hasFile('image')) {
				$user->image = $input['image'];
			}
			$user->save();
			
			return Redirect::to(route('user.show', $user->id));
		} else {
			$v->getMessageBag()->add('oldpassword', 'Password invalid!');
			return Redirect::to(URL::previous())->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function login() {
		$userdata = array(
			'email' => Input::get('loginemail'),
			'password' => Input::get('loginpassword')
		);
		
		if (Auth::attempt($userdata)) {
			Session::put('login_error', '');
			print('success');
			return Redirect::to(url('home'));
		} else {
			Session::put('login_error', 'Login failed');
			print('failed');
			return Redirect::to(URL::previous())->withInput();
		}
	}
	
	public function logout() {
		Auth::logout();
		return Redirect::to(route('home'));
	}
	
	public function search() {
		$input = Input::all();
		$criteria = $input['search'];
		$sql = "select * from users where firstname like ? OR lastname like ? OR email like ? OR firstname + ' ' + lastname like ?";
    	$users = DB::select($sql, array("%$criteria%", "%$criteria%", "%$criteria%", "%$criteria%"));
    	return View::make('user.search')->withUsers($users);
	}
	
	public function friend($id) {
		$user = Auth::user();
		$reference = $user->id;
		$referenced = User::find($id)->id;
		
		if (!is_null($referenced) && !is_null($reference)) {
			$user->friends()->attach($referenced);
		}
		
		return Redirect::to(URL::previous());
	}
}
