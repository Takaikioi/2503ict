<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as'=>'home', 'uses'=> 'PostController@index'));
Route::get('home', array('as'=>'home', 'uses'=> 'PostController@index'));
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::get('user/search', array('as' => 'user.search', 'uses' => 'UserController@search'));
Route::get('user/friend/{friend}', array('as' => 'user.friend', 'uses' => 'UserController@friend'));
Route::resource('user', 'UserController');
Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');















