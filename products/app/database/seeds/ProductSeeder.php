<?php
    class ProductSeeder extends Seeder {
        function run() {
            $product = new Product;
            $product->name = 'Gourmet Can in a Can';
            $product->price = 1.00;
            $product->save();
            
            $product = new Product;
            $product->name = 'Gourmet Can in a Can in a Bag';
            $product->price = 2.00;
            $product->save();
        }
    }
?>