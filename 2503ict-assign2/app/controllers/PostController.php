<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		//select * from posts where user_id = $user->id
		//select * from posts  where privacy='public'
		//select * from posts inner join friends on (friends.reference_id = 8 or friends.referenced_id = 8) and (user_id = friends.reference_id or user_id = friends.referenced_id) where privacy='friends';
		//$user = Auth::user();
		//$friends = $user->friends;
		// $privateposts = Post::where('user_id', '=', $user->id, 'and', 'privacy', '=', 'private');
		// $publicposts = Post::where('privacy', '=', 'public')
		// $friendposts = Post::whereRaw("select * from posts inner join friends on (friends.reference_id = ? or friends.referenced_id = ?) and (user_id = friends.reference_id or user_id = friends.referenced_id) where privacy='friends'", array($user->id, $user->id));
		if (Auth::check()) {
			$posts = Post::orderBy('id', 'DESC')->paginate(10);
		} else {
			$posts = Post::where('privacy', '=', 'public')->orderBy('id', 'DESC')->paginate(10);
		}
	    $commentcount = array();
	    foreach ($posts as $post) {
	        $commentcount[$post->id] = count(Post::find($post->id)->comments);
	    }
		return View::make('post.home') -> withPosts($posts) -> withCommentcount($commentcount);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $post = new Post;
		$input = Input::all();
		$v = Validator::make($input, Post::$rules);
		if (Auth::check()) {
			if ($v->passes()) {	
				$post->user_id = Auth::user()->id;
				$post->title = $input['title'];
				$post->message = $input['message'];
				$post->privacy = $input['privacy'];
				$post->save();
				
				return Redirect::route('post.show', $post->id);
			} else {
				return Redirect::route('post.edit', $post->id)->withErrors($v);
			}
		} else return Redirect::to(url('home'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id);
	    $comments = $post->comments()->paginate(8);
	    $commentcount = count($comments);
	    return View::make('post.show') -> withPost($post) -> withComments($comments) -> withCommentcount($commentcount);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);
		$comments = $post->comments();
	    $commentcount = count($comments);
		return View::make('post.edit', compact('post', 'commentcount'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	
		$post = Post::find($id);
		$input = Input::all();
		$v = Validator::make($input, Post::$rules);
		if (Auth::check()) {
			if ($v->passes()) {	
				$post->user_id = Auth::user()->id;
				$post->title = $input['title'];
				$post->message = $input['message'];
				$post->privacy = $input['privacy'];
				$post->save();
				
				return Redirect::route('post.show', $post->id);
			} else {
				return Redirect::route('post.edit', $post->id)->withErrors($v);
			}
		} else return Redirect::to(url('home'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Post::find($id)->delete();
	    return Redirect::to(url('home'));
	}
}
