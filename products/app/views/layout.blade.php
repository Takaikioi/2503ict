<html>
<head>
    <title>Products</title>
</head>
<body>
    @if (Auth::check())
        {{ Auth::user()->username }} {{ link_to_route('user.logout', "(Sign out)") }}
    @else
        {{ Form::open(array('url' => secure_url('user/login'))) }}
            {{ Form::label('loginusername', 'Username: ') }}
            {{ Form::text('loginusername') }}
            {{ Form::label('loginpassword', 'Password: ') }}
            {{ Form::password('loginpassword') }}
            {{ Form::submit('Login') }}
            <a href = {{ route('user.create') }} >{{ Form::button('Create account') }}</a>
            {{ Form::label('error', Session::get('login_error')) }}
            {{ Session::put('login_error', ' ') }}
        {{ Form::close() }}
    @endif
    @yield('content')
</body>