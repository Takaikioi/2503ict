<?php
    class PostsTableSeeder extends Seeder {
        function run() {
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 1';
            $post->message = 'Message 1';
            $post->privacy = 'public';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 2';
            $post->message = 'Message 2';
            $post->privacy = 'private';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 3';
            $post->message = 'Message 3';
            $post->privacy = 'friends';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 4';
            $post->message = 'Message 4';
            $post->privacy = 'friends';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 5';
            $post->message = 'Message 5';
            $post->privacy = 'friends';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 6';
            $post->message = 'Message 6';
            $post->privacy = 'public';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 7';
            $post->message = 'Message 7';
            $post->privacy = 'public';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 8';
            $post->message = 'Message 8';
            $post->privacy = 'private';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 9';
            $post->message = 'Message 9';
            $post->privacy = 'friends';
            $post->save();
            
            $post = new Post;
            $post->user_id = '10';
            $post->title = 'Title 10';
            $post->message = 'Message 10';
            $post->privacy = 'friends';
            $post->save();
            
            $post = new Post;
            $post->user_id = '2';
            $post->title = 'Title 11';
            $post->message = 'Message 11';
            $post->privacy = 'public';
            $post->save();
            
            $post = new Post;
            $post->user_id = '6';
            $post->title = 'Title 12';
            $post->message = 'Message 12';
            $post->privacy = 'friends';
            $post->save();
            
            $post = new Post;
            $post->user_id = '9';
            $post->title = 'Title 13';
            $post->message = 'Message 13';
            $post->privacy = 'private';
            $post->save();
        }
    }
?>