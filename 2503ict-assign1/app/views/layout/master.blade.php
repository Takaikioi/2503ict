<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    
    <title>Social Network</title>
    
    <!-- Laravel stylesheet links -->
    {{ HTML::style('css/style.css') }}
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/navbar.css') }}
    
    <!-- Laravel script link for boostrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    {{ HTML::script('js/bootstrap.min.js') }}
    
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href='{{{url("home")}}}'>Social Network</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href = '{{{url("home")}}}'>Home</a></li>
              <li><a href = '{{{url("docs/diagrams.png")}}}'>Diagrams</a></li>
              <li><a href = '{{{url("docs/breakdown.docx")}}}'>Work Breakdown</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      
      <!-- Main body -->
      <div class="row">
        <div class="col-sm-4">
          @yield('form')
        </div>
        
        <div class="col-sm-8">
          @yield('content')
        </div>
      </div>

    </div>
  </body>
</html>
