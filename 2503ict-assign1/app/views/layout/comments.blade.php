@extends('layout.master')

@section('form')
    <form method="post" action='{{{ url("postComment") }}}'>
        <div class="form-group">
            <label for "inputName">Name:</label><br>
            <input class="form-control" id="inputName" type="text" name="name" placeholder="Your name here.">
        </div>
        <div class="form-group">
            <label for "inputMessage">Comment:</label><br>
            <textarea id="inputMessage" class="form-control" rows="4" cols="10" name="comment" placeholder="Your comment here."></textarea>
        </div>
        <input type="hidden" name="id" value="{{{ $post->Id }}}">
        <button type="submit" class="btn btn-primary postbtn">Post</button>
    </form>
@stop

@section('content')
    <div>
        <div>
            <div class='post'>
                <div class="imagediv">
                    @if ( File::exists(url("images/$post->Id.jpg")))
                    {{ HTML::image("images/$post->Id.jpg", "image", array("class" => "postimage")) }}
                    @else
                    {{ HTML::image("images/noimage.jpg", "image", array("class" => "postimage")) }}
                    @endif
                </div>
                <div class="textwrap">
                    <label class="username"> {{{$post->User}}} </label><br>
                    <label class="title"> {{{$post->Title}}} </label><br>
                    <label class="message"> {{{$post->Message}}} </label>
                </div>
                <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                    <a href='{{{ url("comments/$post->Id") }}}'><button class="btn btn-info" type="button">Comments <span class="badge">{{{$commentcount[$post->Id]}}}</span></button></a>
                    <a href='{{{ url("edit/$post->Id") }}}'><button class="btn btn-warning">Edit</button></a>
                    <a href='{{{ url("deletep/$post->Id") }}}'><button class="btn btn-danger">Delete</button></a>
                </div>
            </div>
        </div>
    </div>
    <div>
            @foreach ($comments as $comment)
                <div>
                    <div class='post'>
                        <div class="textwrap">
                            <label class="username">{{{$comment->User}}} </label><br>
                            <label class="message">{{{$comment->Comment}}} </label>
                        </div>
                        <div class="btn-group commentcount" style="position: absolute; margin-bottom: 10px;">
                            <a href='{{{ url("deletec/$comment->Id/$post->Id") }}}'><button class="btn btn-danger postbtn">Delete</button></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop